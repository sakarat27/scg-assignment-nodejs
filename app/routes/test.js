const Test = require('../controllers/Test');
exports.init = (app, router) => {

    router.get('/test', Test.index);
    router.get('/test/findX', Test.findX);
    router.get('/test/findXSecondWay', Test.findXSecondWay);
    router.get('/test/findY', Test.findY);
    router.get('/test/findX2', Test.findX2);

};