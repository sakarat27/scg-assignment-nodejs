const express = require('express');
const router = express.Router();
const test = require('./test');

exports.init = (app) => {

    //TEST routes
    test.init(app, router);

    router.get('/', (req, res) => {
        res.send('SCG Assignment <a href="/test">Test</a>')
    });

    return router;

};