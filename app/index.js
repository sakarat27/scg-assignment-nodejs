const express = require('express');
const app = express();
const routes = require('./routes');

app.use('/', routes.init(app));

app.listen(3000, () => {
    console.log('Start server at port 3000.')
});