const cache = require('memory-cache');


exports.index = function(req, res) {
    res.send("<a href='/test/findX'>findX</a> <a href='/test/findY'>findY</a> <a href='/test/findX2'>findX2</a>");
};

function findXresult(input) {
    let output = 0;

    if (cache.get('x:' + input)) {
        console.log('Now get x:' + input + ' from Memory cache');
        output = cache.get('x:' + input);
    } else {
        for (let i = 1; i <= input; i++) {
            output += (i - 1) * 2;
        }
        output += 3;
        cache.put('x:' + input, output);
    }

    return output;
}
exports.findX = function(req, res) {

    let returnStack = [];

    if (cache.get('x')) {
        console.log('Now get X from Memory cache');
        returnStack = cache.get('x');
    } else {
        for (let i = 1; i <= 5; i++) {
            returnStack.push(findXresult(i));
        }

        cache.put('x', returnStack);
    }

    res.send(returnStack.join(", "));
};

exports.findXSecondWay = function(req, res) {

    let returnStack = [];

    if (cache.get('x')) {
        console.log('Now get X from Memory cache');
        returnStack = cache.get('x');
    } else {
        let init = 3;

        for (let i = 1; i <= 5; i++) {
            let num = init + ((i - 1) * 2);
            init = num;

            returnStack.push(num);
        }

        cache.put('x', returnStack);
    }

    res.send(returnStack.join(", "));
};



exports.findY = function(req, res) {

    let y;

    if (cache.get('y')) {
        console.log('Now get Y from Memory cache');
        y = cache.get('y');
    } else {
        y = 99 - 20 - 24;
        cache.put('y', y);
    }

    res.send("Y : " + y + "<br>check : " + ((y + 24) + (10 * 2)));
};

function findX2Result(input) {
    let output = '';

    if (cache.get('x2:' + input)) {
        console.log('Now get x2:' + input + ' from Memory cache');
        output = cache.get('x2:' + input);
    } else {
        for (let i = input; i > 1; i--) {
            output += String(i);
        }

        output += '5';
        cache.put('x2:' + input, output);
    }

    return output;
}
exports.findX2 = function(req, res) {
    let returnStack = [];

    if (cache.get('x2')) {
        console.log('Now get X2 from Memory cache');
        returnStack = cache.get('x2');
    } else {
        for (let i = 1; i <= 5; i++) {
            output = findX2Result(i);
            returnStack.push(i + ' = ' + output);
        }

        cache.put('x2', returnStack);
    }


    res.send(returnStack.join(", "));
};